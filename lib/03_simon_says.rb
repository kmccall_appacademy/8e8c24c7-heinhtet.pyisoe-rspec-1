def echo(string)
string
end

def shout(string)
string.upcase
end

def repeat(string, reps = 1)
answer = []
if reps == 1
  answer = [string]
end
reps.times do
  answer << string
end
answer.join(" ")
end

def start_of_word(word, num)
  answer = []
  word.chars.each_with_index do |el, idx|
    answer << el unless idx > num - 1
  end
  answer.join
end

def first_word(sentence)
  arr = sentence.split(" ")[0]
end

def titleize(title)
  arr = title.split(" ")
  finished = []
  arr.each_with_index do |el, idx|
    if idx == 0
      finished << el.capitalize
    elsif el.length > 3 && el != "over"
      finished << el.capitalize
    else
      finished << el
    end
  end
  finished.join(" ")
end
